<?php

namespace Drupal\email_token\Plugin\Filter;

use Drupal\Core\Url;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * To process the email toKen.
 *
 *  @Filter(
 *    id = "email_token",
 *    title = @Translation("Email Token Filter"),
 *    description = @Translation("Filter to replace email tokens"),
 *    type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *  )
 */
class EmailToken extends FilterBase {

  /**
   * Process the token and langcode.
   */
  public function process($text, $langcode) {
    $url_options = ['absolute' => TRUE];
    if (isset($url_options['langcode'])) {
      $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
      $langcode = $options['langcode'];
    }
    else {
      $langcode = NULL;
    }
    $request = \Drupal::request();
    $route_match = \Drupal::routeMatch();
    $title = \Drupal::service('title_resolver')->getTitle($request, $route_match->getRouteObject());
    $node_url = Url::fromRoute('<current>', [], $url_options)->toString();
    $body_line = 'We appreciate your spreading the word.';
    $mid_mail_link = $this->t('<div class = "mid-email-token"><a href=":url">mail me</a></div>', [':url' => 'mailto:?subject=' . $title . '&body=' . $body_line . ' ' . $node_url]);
    $token_replacement = str_replace([
      '[etf:gin-title]',
      '[etf:gin-url]',
      '[etf:gin-email]',
    ],
    [$title, $node_url, $mid_mail_link], $text);
    return new FilterProcessResult($token_replacement);
  }

}
